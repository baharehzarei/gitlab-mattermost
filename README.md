# GitLab Mattermost 

GitLab Mattermost is a team communication service for sharing messages and
files across PCs and phones, with archiving, instant search and integration 
with GitLab Single-Sign-On. 

*Screenshot: GitLab Mattermost desktop application on Windows with [GitLab theme](https://forum.mattermost.org/t/share-your-favorite-mattermost-theme-colors/1330/5?u=it33)

![alt text](https://gitlab.com/gitlab-org/gitlab-mattermost/raw/master/gitlab_theme.jpg)

### Learn more

- [GitLab Mattermost Administrator's Guide](http://doc.gitlab.com/omnibus/gitlab-mattermost/)
- [GitLab Mattermost Discussion and Troubleshooting Forum](https://forum.mattermost.org/t/the-gitlab-mattermost-troubleshooting-forum/134)
- [Mattermost Documentation](http://docs.mattermost.com/)

### Get the Apps!

[Download iOS, Android, Windows, Mac and Linux apps for GitLab Mattermost](https://about.mattermost.com/downloads/), offering single-sign-on to Mattermost with your GitLab account. 

![Android App](https://gitlab.com/gitlab-org/gitlab-mattermost/raw/master/20160316_gitlab_android_app.png)

### GitLab Mattermost features include: 

#### Messaging and File Sharing 

- Send messages, comments, files and images across public, private and 1-1 channels
- Personalize notifications for unreads and mentions by channel and keyword
- Use #hashtags to tag and find messages, discussions and files  

#### Archiving and Search

- Import Slack user accounts and channel archives
- Search public and private channels for historical messages and comments
- View recent mentions of your name, username, nickname, and custom search terms

#### Anywhere Access

- Use Mattermost from web-enabled PCs and phones
- Attach sound, video and image files from mobile devices
- Define team-specific branding and color themes across your devices

#### Self-Host Ready

- Host and manage dozens of teams from a single server instance behind your firewall
- System Console and command line tools for managing essential IT administration functions
- LDAP/Active Directory connectivity to Mattermost enabled via GitLab Single-Sign-On

### How to Install GitLab Mattermost

- [GitLab Mattermost Install Guide for omnibus](http://doc.gitlab.com/omnibus/gitlab-mattermost/)
- [GitLab Mattermost SSO Documentation](http://docs.mattermost.com/deployment/sso-gitlab.html)
- [GitLab Mattermost Troubleshooting Forum](http://forum.mattermost.org/c/general/gitlab)

### Get Involved:

- [Report issues and get help from GitLab Mattermost Troubleshooting Forum](http://forum.mattermost.org/c/general/gitlab)
- [Please report security issues via Mattermost Responsible Disclosure Policy](http://www.mattermost.org/responsible-disclosure-policy/)
- [Issues not related to GitLab Mattermost or GitLab omnibus](https://about.gitlab.com/getting-help/).
- [Contribute, upvote and discuss new feature ideas](http://www.mattermost.org/feature-ideas/).
- [Read contribution guide and find opportunities to contribute code changes](http://docs.mattermost.com/developer/contribution-guide.html).
